import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

products;

 currentProduct;

  isLoading = true;

 constructor(private _productsService: ProductsService) {
    //this.users = this._userService.getUsers();
  }
  addProduct(product){
    this._productsService.addProduct(product);
  }

 deleteProduct(product){
    //this.posts.splice(
      //this.posts.indexOf(post),1)
    this._productsService.deleteProduct(product);
  }


  ngOnInit() {
        this._productsService.getProducts()
			    .subscribe(products => {this.products = products;
                               this.isLoading = false;
                               console.log(products)});
  }

}
